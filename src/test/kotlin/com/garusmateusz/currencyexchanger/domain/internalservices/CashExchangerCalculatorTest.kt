package com.garusmateusz.currencyexchanger.domain.internalservices

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.math.RoundingMode

internal class CashExchangerCalculatorTest {
    private val cashExchangerCalculator = CashExchangerCalculator()

    companion object {
        private const val DOLLAR_EXCHANGE_RATE = 4.1457
    }

    @Test
    fun convertOneHundredTwentyZlotysToDollarsAndBackAgain_shouldRoundToEqualOrLesserCashAmount() {
        val beginningCashAmountInPln = 123

        val cashAmountInPlnAfterConversionToDollars = BigDecimal(beginningCashAmountInPln - 3)
        val cashAmountInUsdAfterConversionToDollars = cashExchangerCalculator.decreaseCashAmountForForeignCurrency(
            cashAmountInPlnAfterConversionToDollars, DOLLAR_EXCHANGE_RATE
        )

        assertThat(BigDecimal(beginningCashAmountInPln).minus(cashAmountInPlnAfterConversionToDollars))
            .isEqualTo(BigDecimal(3))
        assertThat(cashAmountInUsdAfterConversionToDollars)
            .isEqualTo(BigDecimal(28.94).setScale(2, RoundingMode.HALF_DOWN))

        val cashAmountInPlnAfterSecondConversionFromDollarsBackAgain = cashExchangerCalculator
            .increaseCashAmountForForeignCurrency(cashAmountInUsdAfterConversionToDollars, DOLLAR_EXCHANGE_RATE)

        assertThat(cashAmountInPlnAfterSecondConversionFromDollarsBackAgain.plus(BigDecimal(3)))
            .isEqualTo(BigDecimal(122.97).setScale(2, RoundingMode.HALF_DOWN))
    }
}