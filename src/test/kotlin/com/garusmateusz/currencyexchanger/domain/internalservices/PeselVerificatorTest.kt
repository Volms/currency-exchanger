package com.garusmateusz.currencyexchanger.domain.internalservices

import com.garusmateusz.currencyexchanger.domain.models.UnknownCenturyException
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.LocalDate

internal class PeselVerificatorTest {
    @Test
    fun validateIfPersonIsAdult_shouldReturnTrue() {
        val result = PeselVerificator(DATE).validateIfPersonIsAdult("92030606075")

        assertTrue(result)
    }

    @Test
    fun validateIfPersonIsAdult_shouldReturnFalse() {
        val pastDateWhenIHadHairsOnMyHead = LocalDate.of(2001, 11, 17)

        val result = PeselVerificator(pastDateWhenIHadHairsOnMyHead).validateIfPersonIsAdult("92030606075")

        assertFalse(result)
    }

    @Test
    fun validateIfPersonIsAdult_personBornIn1889_shouldReturnFalse() {
        val result = PeselVerificator(DATE).validateIfPersonIsAdult("89830187442")

        assertTrue(result)
    }

    @Test
    fun validateIfPersonIsAdult_personBornIn2071_shouldReturnFalse() {
        val result = PeselVerificator(DATE).validateIfPersonIsAdult("71301651445")

        assertFalse(result)
    }

    @Test
    fun validateIfPersonIsAdult_personWhoWillBe18Tomorrow_shouldReturnFalse() {
        val result = PeselVerificator(DATE).validateIfPersonIsAdult("03311855555")

        assertFalse(result)
    }

    @Test
    fun validateIfPersonIsAdult_personWhoIs18Today_shouldReturnFalse() {
        val result = PeselVerificator(DATE).validateIfPersonIsAdult("03311755555")

        assertFalse(result)
    }

    @Test
    fun validateIfPersonIsAdult_unknownDateDueWrongPeselFormat_shouldThrowException() {
        assertThrows<UnknownCenturyException> {
            PeselVerificator(DATE).validateIfPersonIsAdult("03951755555")
        }
    }

    companion object {
        val DATE = LocalDate.of(2021, 11, 17)
    }
}