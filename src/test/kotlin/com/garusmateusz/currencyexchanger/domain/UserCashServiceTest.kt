package com.garusmateusz.currencyexchanger.domain

import com.garusmateusz.currencyexchanger.adapters.apis.NbpClient
import com.garusmateusz.currencyexchanger.adapters.persistence.UserAccountManipulationRepository
import com.garusmateusz.currencyexchanger.domain.internalservices.CashExchangerCalculator
import com.garusmateusz.currencyexchanger.domain.internalservices.PeselVerificator
import com.garusmateusz.currencyexchanger.domain.models.Currency
import com.garusmateusz.currencyexchanger.domain.models.NoPlnInExchangeableCurrenciesException
import com.garusmateusz.currencyexchanger.domain.models.UserAccountDetails
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.math.BigDecimal

internal class UserCashServiceTest {
    private val userAccountManipulationRepository: UserAccountManipulationRepository = mock()
    private val peselVerificator: PeselVerificator = mock()
    private val nbpClient: NbpClient = mock()
    private val cashExchangerCalculator: CashExchangerCalculator = mock()

    private val userCashService = UserCashService(
        userAccountManipulationRepository,
        peselVerificator,
        nbpClient,
        cashExchangerCalculator
    )

    companion object {
        private const val PESEL = "PESEL"
    }

    @Test
    fun userExists_peselExists_returnsTrue() {
        given(userAccountManipulationRepository.userExists(PESEL)).willReturn(true)

        val result = userCashService.userExists(PESEL)

        assertTrue(result)
    }

    @Test
    fun userExists_peselDoesNotExists_returnsFalse() {
        given(userAccountManipulationRepository.userExists(PESEL)).willReturn(false)

        val result = userCashService.userExists(PESEL)

        assertFalse(result)
    }

    @Test
    fun isUserAdult_userIsAdult_returnsTrue() {
        given(peselVerificator.validateIfPersonIsAdult(PESEL)).willReturn(true)

        val result = userCashService.isUserAdult(PESEL)

        assertTrue(result)
    }

    @Test
    fun userExists_userIsNotAdult_returnsFalse() {
        given(peselVerificator.validateIfPersonIsAdult(PESEL)).willReturn(false)

        val result = userCashService.isUserAdult(PESEL)

        assertFalse(result)
    }

    @Test
    fun getUserDetails_returnsDetails() {
        val userAccountDetails = UserAccountDetails(
            "John",
            "Kovalski",
            PESEL,
            mutableSetOf()
        )
        given(userAccountManipulationRepository.getUserDetails(PESEL)).willReturn(userAccountDetails)

        val result = userCashService.getUserDetails(PESEL)

        assertThat(result).isEqualTo(userAccountDetails)
    }

    @Test
    fun exchangeCash_exchangePLNtoUSD_shouldExchange() {
        val amountInFromCurrency = BigDecimal(5.0)
        val decreasedCashAmountForNativeCurrency = BigDecimal(-1)
        val decreasedCashAmountForForeignCurrency = BigDecimal(2)
        val exchangeRate = 5.0

        given(nbpClient.getExchangeRate(Currency.USD.toIsoCode())).willReturn(exchangeRate)
        given(cashExchangerCalculator.decreaseCashAmountForNativeCurrency(amountInFromCurrency))
            .willReturn(decreasedCashAmountForNativeCurrency)
        given(cashExchangerCalculator.decreaseCashAmountForForeignCurrency(amountInFromCurrency, exchangeRate))
            .willReturn(decreasedCashAmountForForeignCurrency)

        userCashService.exchangeCash(
            PESEL,
            fromCurrency = Currency.PLN,
            amountInFromCurrency = amountInFromCurrency,
            toCurrency = Currency.USD
        )

        verify(userAccountManipulationRepository).createAccountInCurrencyIfDoesNotExistYet(PESEL, Currency.USD)
        verify(userAccountManipulationRepository).changeCashAmountOnAccount(PESEL, decreasedCashAmountForNativeCurrency, Currency.PLN)
        verify(userAccountManipulationRepository).changeCashAmountOnAccount(PESEL, decreasedCashAmountForForeignCurrency, Currency.USD)
    }

    @Test
    fun exchangeCash_exchangeUSDtoPLN_shouldExchange() {
        val amountInFromCurrency = BigDecimal(5.0)
        val increasedCashAmountForForeignCurrency = BigDecimal(-1)
        val decreasedCashAmountForNativeCurrency = BigDecimal(2)
        val exchangeRate = 5.0

        given(nbpClient.getExchangeRate(Currency.USD.toIsoCode())).willReturn(exchangeRate)
        given(cashExchangerCalculator.increaseCashAmountForForeignCurrency(amountInFromCurrency, exchangeRate))
            .willReturn(increasedCashAmountForForeignCurrency)
        given(cashExchangerCalculator.decreaseCashAmountForNativeCurrency(amountInFromCurrency))
            .willReturn(decreasedCashAmountForNativeCurrency)

        userCashService.exchangeCash(
            PESEL,
            fromCurrency = Currency.USD,
            amountInFromCurrency = amountInFromCurrency,
            toCurrency = Currency.PLN
        )

        verify(userAccountManipulationRepository).createAccountInCurrencyIfDoesNotExistYet(PESEL, Currency.PLN)
        verify(userAccountManipulationRepository).changeCashAmountOnAccount(PESEL, increasedCashAmountForForeignCurrency, Currency.PLN)
        verify(userAccountManipulationRepository).changeCashAmountOnAccount(PESEL, decreasedCashAmountForNativeCurrency, Currency.USD)
    }

    @Test
    fun exchangeCash_noPLNcurrencyInTrade_shouldThrowException() {
        assertThatThrownBy {
            userCashService.exchangeCash(
                PESEL,
                fromCurrency = Currency.USD,
                amountInFromCurrency = BigDecimal(1.0),
                toCurrency = Currency.USD
            )
        }.isInstanceOf(NoPlnInExchangeableCurrenciesException::class.java)
    }
}