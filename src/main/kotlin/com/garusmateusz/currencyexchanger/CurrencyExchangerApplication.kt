package com.garusmateusz.currencyexchanger

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients

@EnableFeignClients
@SpringBootApplication
class CurrencyExchangerApplication

fun main(args: Array<String>) {
    runApplication<CurrencyExchangerApplication>(*args)
}
