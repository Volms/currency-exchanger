package com.garusmateusz.currencyexchanger.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.LocalDate

@Configuration
class AppConfig {

    @Bean
    fun localDate() = LocalDate.now()
}