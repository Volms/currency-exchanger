package com.garusmateusz.currencyexchanger.adapters.apis

import com.garusmateusz.currencyexchanger.adapters.apis.lowlevel.LowLevelNbpClient
import com.garusmateusz.currencyexchanger.domain.ports.INbpClient
import org.springframework.stereotype.Component

@Component
class NbpClient(
    private val lowLevelNbpClient: LowLevelNbpClient
) : INbpClient {
    override fun getExchangeRate(isoCode: String): Double {
        val result = lowLevelNbpClient.getExchangeRate(isoCode)

        return result.rates.first().mid
    }
}