package com.garusmateusz.currencyexchanger.adapters.apis.lowlevel

import com.garusmateusz.currencyexchanger.adapters.apis.lowlevel.models.ExchangeRateResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET

@FeignClient(
    name = "NBP",
    url = "http://api.nbp.pl"
)
interface LowLevelNbpClient {
    @RequestMapping(
        method = [GET],
        value = ["/api/exchangerates/rates/A/{currency}"],
        produces = ["application/json"]
    )
    fun getExchangeRate(@PathVariable("currency") currency: String): ExchangeRateResponse
}