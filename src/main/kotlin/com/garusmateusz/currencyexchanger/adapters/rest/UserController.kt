package com.garusmateusz.currencyexchanger.adapters.rest

import com.garusmateusz.currencyexchanger.domain.models.Currency
import com.garusmateusz.currencyexchanger.domain.UserCashService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import java.net.URI

@RestController
@RequestMapping("user")
class UserController(
    private val userCashService: UserCashService
) {
    @RequestMapping("/register", method = [RequestMethod.PUT])
    fun registerUser(@RequestBody request: RegisterUserRequest): ResponseEntity<Any> {
        if(!request.pesel.matches(Regex("[0-9]{11}"))) {
            return ResponseEntity.status(409).body("Wrong PESEL, should be made of 11 digits.")
        }

        if(!request.initialBalanceInPLN.matches(Regex("\\d+(\\.\\d+)?"))) {
            return ResponseEntity.status(409).body("Wrong format of initial balance, should be digit.")
        }

        val initialValueInBigDecimal = BigDecimal(request.initialBalanceInPLN)

        if (!userCashService.isUserAdult(request.pesel)) {
            return ResponseEntity.status(409).body("Wrong PESEL, person is underage.")
        }

        if (initialValueInBigDecimal < BigDecimal.ZERO) {
            return ResponseEntity.status(409).body("Initial balance cannot be lesser than 0.")
        }

        if (userCashService.userExists(request.pesel)) {
            return ResponseEntity.status(409).body("Given user already exists.")
        }

        userCashService.createUserWithBasicAccount(
            request.name,
            request.surname,
            request.pesel,
            initialValueInBigDecimal
        )

        return ResponseEntity.created(URI.create("/user/${request.pesel}"))
            .body(RegisterUserResponse(request.pesel))
    }

    @RequestMapping("/{pesel}", method = [RequestMethod.GET])
    fun getAccountDetails(@PathVariable pesel: String): ResponseEntity<Any> {
        if (!userCashService.userExists(pesel)) {
            return ResponseEntity.status(404).body("Given user doesn't exists.")
        }

        val response = userCashService.getUserDetails(pesel)

        return ResponseEntity.ok(
            UserAccountDetailsResponse(
                name = response.name,
                surname = response.surname,
                pesel = response.pesel,
                accounts = response.accounts.map {
                    Account(
                        balance = it.balance.toPlainString(),
                        currency = it.currency.toString()
                    )
                }.toSet()
            )
        )
    }

    @RequestMapping(
        "/{pesel}/exchange/{fromCurrency}/{amountInFromCurrency}/{toCurrency}",
        method = [RequestMethod.POST]
    )
    fun exchangeCurrency(
        @PathVariable pesel: String,
        @PathVariable fromCurrency: String,
        @PathVariable amountInFromCurrency: String,
        @PathVariable toCurrency: String
    ): ResponseEntity<Any> {
        if (!userCashService.userExists(pesel)) {
            return ResponseEntity.status(404).body("Given user doesn't exists.")
        }

        if(!amountInFromCurrency.matches(Regex("\\d+(\\.\\d+)?"))) {
            return ResponseEntity.status(409).body("Wrong format of amount, should be digit.")
        }

        if (amountInFromCurrency.toDouble() <= 0) {
            return ResponseEntity.status(409).body("Requested amount should be greater than 0.")
        }

        if (!userCashService.isCurrencySupported(fromCurrency)) {
            return ResponseEntity.status(409).body("Given currency $fromCurrency is not supported.")
        }

        if (!userCashService.isCurrencySupported(toCurrency)) {
            return ResponseEntity.status(409).body("Given currency $toCurrency is not supported.")
        }

        if (!userCashService.verifyIfUserHasRequiredBalance(
                pesel,
                requiredBalance = BigDecimal(amountInFromCurrency),
                currency = Currency.fromString(fromCurrency)
            )
        ) {
            return ResponseEntity.status(409).body("Users does not have enough balance in $fromCurrency.")
        }

        userCashService.exchangeCash(
            pesel = pesel,
            fromCurrency = Currency.fromString(fromCurrency),
            amountInFromCurrency = BigDecimal(amountInFromCurrency),
            toCurrency = Currency.fromString(toCurrency)
        )

        return ResponseEntity.ok().build()
    }
}
