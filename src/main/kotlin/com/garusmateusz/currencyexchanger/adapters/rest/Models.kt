package com.garusmateusz.currencyexchanger.adapters.rest

data class RegisterUserRequest(
    val name: String,
    val surname: String,
    val pesel: String,
    val initialBalanceInPLN: String
)

data class RegisterUserResponse(val pesel: String)

data class UserAccountDetailsResponse(
    val name: String,
    val surname: String,
    val pesel: String,
    val accounts: Set<Account>
)

data class Account(
    val balance: String,
    val currency: String
)