package com.garusmateusz.currencyexchanger.adapters.persistence.lowlevel

import com.garusmateusz.currencyexchanger.adapters.persistence.lowlevel.models.ServiceUser
import org.springframework.data.repository.CrudRepository

interface LowLevelUserRepository : CrudRepository<ServiceUser, String>