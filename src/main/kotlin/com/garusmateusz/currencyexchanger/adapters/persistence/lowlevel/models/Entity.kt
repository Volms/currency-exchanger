package com.garusmateusz.currencyexchanger.adapters.persistence.lowlevel.models

import java.math.BigDecimal
import javax.persistence.CollectionTable
import javax.persistence.ElementCollection
import javax.persistence.Embeddable
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id
import javax.persistence.JoinColumn

@Entity(name = "service_user")
data class ServiceUser(
    @Id
    val pesel: String,
    val name: String,
    val surname: String,
    @ElementCollection
    @CollectionTable(
        name = "USER_ACCOUNTS",
        joinColumns = [JoinColumn(name = "pesel")]
    )
    val accounts: MutableSet<Account>
)

@Embeddable
data class Account(
    @Enumerated(EnumType.STRING)
    val currency: Currency,
    var amount: BigDecimal
)

enum class Currency {
    PLN,
    USD
}