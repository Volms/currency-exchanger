package com.garusmateusz.currencyexchanger.adapters.persistence

import com.garusmateusz.currencyexchanger.domain.models.UserAccountDetails
import com.garusmateusz.currencyexchanger.domain.models.toDomainCurrency
import com.garusmateusz.currencyexchanger.domain.models.toEntityCurrency
import com.garusmateusz.currencyexchanger.adapters.persistence.lowlevel.models.Account
import com.garusmateusz.currencyexchanger.adapters.persistence.lowlevel.LowLevelUserRepository
import com.garusmateusz.currencyexchanger.adapters.persistence.lowlevel.models.ServiceUser
import com.garusmateusz.currencyexchanger.domain.ports.IUserRepository
import org.springframework.stereotype.Service
import java.math.BigDecimal
import com.garusmateusz.currencyexchanger.domain.models.Account as DomainAccount
import com.garusmateusz.currencyexchanger.domain.models.Currency as DomainCurrency

@Service
class UserAccountManipulationRepository(
    private val lowLevelUserRepository: LowLevelUserRepository
) : IUserRepository {
    override fun createUser(name: String, surname: String, pesel: String) {
        lowLevelUserRepository.save(ServiceUser(pesel = pesel, name = name, surname = surname, accounts = mutableSetOf()))
    }

    override fun userExists(pesel: String) = lowLevelUserRepository.existsById(pesel)

    override fun getUserDetails(pesel: String): UserAccountDetails {
        val userAccountDetails = lowLevelUserRepository.findById(pesel).get()

        return UserAccountDetails(
            name = userAccountDetails.name,
            surname = userAccountDetails.surname,
            pesel = userAccountDetails.pesel,
            accounts = userAccountDetails.accounts.map {
                DomainAccount(
                    balance = it.amount,
                    currency = it.currency.toDomainCurrency()
                )
            }.toMutableSet()
        )
    }

    override fun createAccountInCurrencyIfDoesNotExistYet(pesel: String, currency: DomainCurrency) {
        val userAccountDetails = lowLevelUserRepository.findById(pesel).get()
        if(!userAccountDetails.accounts.any { it.currency.toDomainCurrency() == currency }) {
            userAccountDetails.accounts.add(Account(currency.toEntityCurrency(), BigDecimal.ZERO))
        }
    }

    override fun changeCashAmountOnAccount(pesel: String, byAmount: BigDecimal, currency: DomainCurrency) {
        val userAccountDetails = lowLevelUserRepository.findById(pesel).get()
        userAccountDetails.accounts.first { it.currency.toDomainCurrency() == currency }.amount += byAmount

        lowLevelUserRepository.save(userAccountDetails)
    }

    override fun assignCashToUser(pesel: String, amount: BigDecimal, currency: DomainCurrency) {
        val userAccountDetails = lowLevelUserRepository.findById(pesel).get()

        if (userAccountDetails.accounts.any { it.currency.toString() == currency.toString() }) {
            userAccountDetails.accounts.first { it.currency.toString() == currency.toString() }.let {
                val currentBalance = it.amount
                it.amount = currentBalance + amount
            }
        } else {
            userAccountDetails.accounts.add(
                Account(
                    currency.toEntityCurrency(),
                    amount
                )
            )
        }

        lowLevelUserRepository.save(userAccountDetails)
    }
}