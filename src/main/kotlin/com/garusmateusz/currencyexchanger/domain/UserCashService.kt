package com.garusmateusz.currencyexchanger.domain

import com.garusmateusz.currencyexchanger.domain.internalservices.CashExchangerCalculator
import com.garusmateusz.currencyexchanger.domain.internalservices.PeselVerificator
import com.garusmateusz.currencyexchanger.domain.internalservices.TransactionHandler
import com.garusmateusz.currencyexchanger.domain.models.Currency
import com.garusmateusz.currencyexchanger.domain.models.NoPlnInExchangeableCurrenciesException
import com.garusmateusz.currencyexchanger.domain.ports.INbpClient
import com.garusmateusz.currencyexchanger.domain.ports.IUserRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal

@Service
class UserCashService(
    private val userAccountManipulationRepository: IUserRepository,
    private val peselVerificator: PeselVerificator,
    private val nbpClient: INbpClient,
    private val cashExchangerCalculator: CashExchangerCalculator
) {
    private val transactionHandler: TransactionHandler = TransactionHandler()

    fun userExists(pesel: String) = userAccountManipulationRepository.userExists(pesel)

    fun isUserAdult(pesel: String) = peselVerificator.validateIfPersonIsAdult(pesel)

    fun getUserDetails(pesel: String) = userAccountManipulationRepository.getUserDetails(pesel)

    fun verifyIfUserHasRequiredBalance(pesel: String, requiredBalance: BigDecimal, currency: Currency): Boolean {
        val currentBalance = userAccountManipulationRepository.getUserDetails(pesel).accounts
            .firstOrNull { it.currency == currency }?.balance ?: return false

        return currentBalance >= requiredBalance
    }

    fun exchangeCash(pesel: String, fromCurrency: Currency, amountInFromCurrency: BigDecimal, toCurrency: Currency) {
        val foreignCurrencyExchangeRate = when {
            fromCurrency == Currency.PLN -> nbpClient.getExchangeRate(toCurrency.toIsoCode())
            toCurrency == Currency.PLN -> nbpClient.getExchangeRate(fromCurrency.toIsoCode())
            else -> throw NoPlnInExchangeableCurrenciesException("One of fromCurrency[$fromCurrency] or toCurrency[$toCurrency] should be in PLN.")
        }

        transactionHandler.executeInTransaction {
            processCurrencyExchange(pesel, fromCurrency, amountInFromCurrency, toCurrency, foreignCurrencyExchangeRate)
        }
    }

    private fun processCurrencyExchange(
        pesel: String,
        fromCurrency: Currency,
        amountInFromCurrency: BigDecimal,
        toCurrency: Currency,
        foreignCurrencyExchangeRate: Double
    ) {
        userAccountManipulationRepository.createAccountInCurrencyIfDoesNotExistYet(pesel, toCurrency)

        if (fromCurrency == Currency.PLN) {
            userAccountManipulationRepository.changeCashAmountOnAccount(
                pesel,
                byAmount = cashExchangerCalculator.decreaseCashAmountForNativeCurrency(amountInFromCurrency),
                Currency.PLN
            )
            userAccountManipulationRepository.changeCashAmountOnAccount(
                pesel,
                byAmount = cashExchangerCalculator.decreaseCashAmountForForeignCurrency(
                    amountInFromCurrency,
                    foreignCurrencyExchangeRate
                ),
                toCurrency
            )
        } else {
            userAccountManipulationRepository.changeCashAmountOnAccount(
                pesel,
                byAmount = cashExchangerCalculator.increaseCashAmountForForeignCurrency(
                    amountInFromCurrency,
                    foreignCurrencyExchangeRate
                ),
                Currency.PLN
            )
            userAccountManipulationRepository.changeCashAmountOnAccount(
                pesel,
                byAmount = cashExchangerCalculator.decreaseCashAmountForNativeCurrency(amountInFromCurrency),
                fromCurrency
            )
        }
    }

    fun isCurrencySupported(currency: String): Boolean {
        return when (currency) {
            "USD" -> true
            "PLN" -> true
            else -> false
        }
    }

    @Transactional
    fun createUserWithBasicAccount(name: String, surname: String, pesel: String, initialBalanceInPLN: BigDecimal) {
        userAccountManipulationRepository.createUser(name, surname, pesel)
        userAccountManipulationRepository.assignCashToUser(pesel, initialBalanceInPLN, Currency.PLN)
    }
}
