package com.garusmateusz.currencyexchanger.domain.models
import com.garusmateusz.currencyexchanger.adapters.persistence.lowlevel.models.Currency as EntityCurrency
import com.garusmateusz.currencyexchanger.domain.models.Currency as DomainCurrency

fun DomainCurrency.toEntityCurrency(): EntityCurrency {
    return when(this) {
        DomainCurrency.PLN -> EntityCurrency.PLN
        DomainCurrency.USD -> EntityCurrency.USD
    }
}

fun EntityCurrency.toDomainCurrency(): DomainCurrency {
    return when(this) {
        EntityCurrency.PLN -> DomainCurrency.PLN
        EntityCurrency.USD -> DomainCurrency.USD
    }
}