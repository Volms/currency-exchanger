package com.garusmateusz.currencyexchanger.domain.models

class UnsupportedCurrencyException : RuntimeException()
class NoPlnInExchangeableCurrenciesException(message: String) : RuntimeException(message)
class UnknownCenturyException : RuntimeException()