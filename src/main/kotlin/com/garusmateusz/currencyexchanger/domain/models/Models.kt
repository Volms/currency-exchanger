package com.garusmateusz.currencyexchanger.domain.models

import java.math.BigDecimal

data class UserAccountDetails(
    val name: String,
    val surname: String,
    val pesel: String,
    val accounts: MutableSet<Account>
)

data class Account(
    val balance: BigDecimal,
    val currency: Currency
)

enum class Currency {
    PLN, USD;

    fun toIsoCode(): String {
        return when(this) {
            PLN -> "PLN"
            USD -> "USD"
        }
    }

    companion object {
        fun fromString(value: String): Currency {
            return when (value) {
                "PLN" -> PLN
                "USD" -> USD
                else -> throw UnsupportedCurrencyException()
            }
        }
    }
}

