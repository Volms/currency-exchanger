package com.garusmateusz.currencyexchanger.domain.internalservices

import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.math.RoundingMode

@Component
class CashExchangerCalculator {
    companion object {
        private val minusOneInBigDecimal = BigDecimal(-1)
    }

    fun decreaseCashAmountForNativeCurrency(amount: BigDecimal): BigDecimal {
        return amount.multiply(minusOneInBigDecimal)
    }

    fun decreaseCashAmountForForeignCurrency(amount: BigDecimal, foreignCurrencyExchangeRate: Double): BigDecimal {
        val convertedExchangeRateToBigDecimal = BigDecimal(foreignCurrencyExchangeRate).setScale(4, RoundingMode.DOWN)
        return amount.divide(convertedExchangeRateToBigDecimal, 2, RoundingMode.DOWN)
    }

    fun increaseCashAmountForForeignCurrency(amount: BigDecimal, foreignCurrencyExchangeRate: Double): BigDecimal {
        val convertedExchangeRateToBigDecimal = BigDecimal(foreignCurrencyExchangeRate).setScale(4, RoundingMode.DOWN)
        return amount.multiply(convertedExchangeRateToBigDecimal).setScale(2, RoundingMode.HALF_UP)
    }
}