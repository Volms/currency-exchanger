package com.garusmateusz.currencyexchanger.domain.internalservices

import com.garusmateusz.currencyexchanger.domain.models.UnknownCenturyException
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class PeselVerificator(
    private val currentDate: LocalDate
) {
    fun validateIfPersonIsAdult(pesel: String): Boolean {
        val yearInCentury = pesel.slice(0..1)
        val personBirthYear = when (pesel.slice(2..3).toInt()) {
            in 1..12 -> "19$yearInCentury"
            in 21..32 -> "20$yearInCentury"
            in 41..52 -> "21$yearInCentury"
            in 61..72 -> "22$yearInCentury"
            in 81..92 -> "18$yearInCentury"
            else -> throw UnknownCenturyException()
        }

        val monthInCentury = pesel.slice(2..3)
        val personBirthMonth = when(monthInCentury[0].digitToInt() % 2 == 0) {
            true -> monthInCentury[1].toString()
            false -> "1${monthInCentury[1]}"
        }

        val personBirthDay = pesel.slice(4..5)

        val personBirthDate = LocalDate.of(personBirthYear.toInt(), personBirthMonth.toInt(), personBirthDay.toInt())

        return personBirthDate.isBefore(currentDate.minusYears(18))
    }
}
