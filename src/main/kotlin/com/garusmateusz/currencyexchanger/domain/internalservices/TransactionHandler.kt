package com.garusmateusz.currencyexchanger.domain.internalservices

import org.springframework.transaction.annotation.Transactional

open class TransactionHandler {
    @Transactional
    open fun <T> executeInTransaction(block: () -> T):T {
        return block()
    }
}