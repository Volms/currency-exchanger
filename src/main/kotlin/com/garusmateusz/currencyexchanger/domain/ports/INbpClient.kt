package com.garusmateusz.currencyexchanger.domain.ports

interface INbpClient {
    fun getExchangeRate(isoCode: String): Double
}