package com.garusmateusz.currencyexchanger.domain.ports

import com.garusmateusz.currencyexchanger.domain.models.Currency
import com.garusmateusz.currencyexchanger.domain.models.UserAccountDetails
import java.math.BigDecimal

interface IUserRepository {
    fun createUser(name: String, surname: String, pesel: String)
    fun userExists(pesel: String): Boolean
    fun getUserDetails(pesel: String): UserAccountDetails
    fun createAccountInCurrencyIfDoesNotExistYet(pesel: String, currency: Currency)
    fun changeCashAmountOnAccount(pesel: String, byAmount: BigDecimal, currency: Currency)
    fun assignCashToUser(pesel: String, amount: BigDecimal, currency: Currency)
}